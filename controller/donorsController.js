const validator = require('validator');
const mongoose = require('mongoose');
const DonorInfo = require('../model/donorModel');
mongoose.connect('mongodb://localhost/authWork');

class Donors {
    static saveDonorData(req, res) {
        let donorInfo = new DonorInfo(req.body);

        let data = req.body;
        if (
            data.name.trim() !== "" &&
            data.name.trim().length >= 3 &&
            validator.isEmail(data.email) &&
            data.contactNo.toString().trim() !== "" &&
            data.contactNo.toString().length >= 4 &&
            data.bloodGroup.trim() !== "" &&
            data.bloodGroup.trim().length > 0 && data.bloodGroup.trim().length <= 3 &&
            data.id.toString().trim() !== ""
        ) {
            donorInfo.save((err, user) => {
                if (!err) {
                    return res.send({ user });
                }
                return res.status(404).send({ message: err });
            });
        } else {
            return res.sendStatus(403).send({ message: 'Your data is badley formated' });
        }
    }
}

module.exports = Donors;