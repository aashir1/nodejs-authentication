const bcrypt = require('bcrypt');
const User = require('../model/userModel');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const validator = require('validator');

mongoose.connect('mongodb://localhost/authWork');
class Controller{
    static register(req, res){
        let userDocument = new User(req.body);
        console.log(req.body);
        if(validator.isEmail(req.body.email) && req.body.password.trim() !== ""){
            userDocument.password = bcrypt.hashSync(req.body.password, 10);
            userDocument.save((err, user) =>{
                if(!err){
                    user.password = undefined;
                    return res.json({token: jwt.sign({name: user.name, email: user.email, _id: user._id, created: user.created},'RESTFULAPIS'), _user: user});                
                }
                return res.status(400).send({message: err});
            });
        }
        else{
            return res.status(400).json({message: 'Email or password badly formated'});
        }
    };

    static signIn(req, res){
        if(validator.isEmail(req.body.email) && req.body.password.trim() !== ""){
            User.findOne({email: req.body.email}, (err, user)=>{
                if(!user){
                    res.status(401).json({message: "Authentication failed user not found"});
                }
                if(user){
                    if(bcrypt.compareSync(req.body.password, user.password)){
                        user.password = undefined;
                        return res.json({token: jwt.sign({name: user.name, email: user.email, _id: user._id, created: user.created}, 'RESTFULAPIS'), _user: user});
                    }
                    return res.status(401).json({message: "Wrong password detected"});
                }
            })
        }else{
            return res.status(400).json({message: 'Email or password badly formated'});
        }
    }
    static signOut(req, res){

    }
};

module.exports = Controller;