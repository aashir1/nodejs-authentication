const mongoose = require('mongoose');

const donorSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        require: true,
        unique: true
    },
    contactNo: {
        type: Number,
        require: true,
        unique: true
    },
    bloodGroup: {
        type: String,
        require: true
    },
    created: {
        type: Date,
        default: Date.now
    }
},{autoIndex: false});
donorSchema.index({
    type: String,
    required: true,
    index: true
})
// donorSchema.index({_id: 1}, {sparse: true});
let DonorInfo = mongoose.model('DonorInfo', donorSchema);
module.exports = DonorInfo;