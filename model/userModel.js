const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const userSchema = mongoose.Schema({
    name: {
        type: String,
        require: true,
        trim: true,
    },
    email:{
        type: String,
        unique: true,
        require: true,
        trim: true
    },
    password:{
        type: String,
        require: true
    },
    created:{
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('User', userSchema);