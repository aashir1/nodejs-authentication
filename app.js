const express = require('express');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const Controller = require('./controller/authController');
const User = require('./model/userModel');
const Donors = require('./controller/donorsController');
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

app.post('/register', function (req, res) {
    Controller.register(req, res);
});

app.post('/signin', function (req, res) {
    Controller.signIn(req, res);
});

//get all users
app.post('/allUsers', verifyToken, (req, res) => {
    jwt.verify(req.token, 'RESTFULAPIS', (err, result) => {
        if (!err) {
            User.find({}, (err, result) => {
                if (err) {
                    res.json({ message: err });
                }
                res.json(result);
            });
        } else {
            res.status(400).json({ message: err });
        }
    });
});

//add donor

app.post('/add_donor', verifyToken, (req, res) => {
    jwt.verify(req.token, 'RESTFULAPIS', (err, result) => {
        if (!err) {
            Donors.saveDonorData(req, res);
        } else {
            console.log('error aya: ' + err);
            return res.status(404).json({ message: "Wrong token detected", name: err.name });
        }
    })
})

function verifyToken(req, res, next) {
    let token = req.headers['authorization'].split(' ')[1];
    if (token && token !== 'undefined') {
        req.token = token;
        next();
    } else {
        res.status(403).send({ message: 'illegal access or access denied' });
    }
}

app.listen(3000, () => {
    console.log('app listen on port 3000 . ....');
});